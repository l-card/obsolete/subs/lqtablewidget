#ifndef LQTABLEWIDGET_H
#define LQTABLEWIDGET_H

#include <QTableWidget>
class LQTableCopyAction;

class LQTableWidget : public QTableWidget {
    Q_OBJECT
public:
    explicit LQTableWidget(QWidget *parent = 0);
    explicit LQTableWidget(int rows, int columns, QWidget *parent = 0);

private:
    void init();
    LQTableCopyAction *m_cpyEvent;
};

#endif // LQTABLEWIDGET_H
