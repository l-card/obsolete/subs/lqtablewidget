#ifndef LQTABLECOPYACTION_H
#define LQTABLECOPYACTION_H

#include <QAction>
class QTableView;
class QEvent;

class LQTableCopyAction : public QAction {
    Q_OBJECT
public:
    LQTableCopyAction(QTableView *parent);
    ~LQTableCopyAction();



public slots:
    void copy();
    void retranslate();
private:
    QTableView *m_view;
};

#endif // LQTABLECOPYACTION_H
