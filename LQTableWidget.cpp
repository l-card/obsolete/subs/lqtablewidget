#include "LQTableWidget.h"
#include "LQTableCopyAction.h"

LQTableWidget::LQTableWidget(QWidget *parent) :
    QTableWidget(parent) {
    init();
}

LQTableWidget::LQTableWidget(int rows, int columns, QWidget *parent)
    : QTableWidget(rows, columns, parent) {
    init();
}

void LQTableWidget::init() {
    m_cpyEvent = new LQTableCopyAction(this);
    addAction(m_cpyEvent);
}


